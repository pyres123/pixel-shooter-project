﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class groundCheck : MonoBehaviour {

	playerController Player;

	void Start()
	{
		Player = gameObject.GetComponentInParent<playerController> ();
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		Player.isGrounded = true;
	}

	void OnTriggerStay2D(Collider2D coll)
	{
		Player.isGrounded = true;
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		Player.isGrounded = false;
	}
}
