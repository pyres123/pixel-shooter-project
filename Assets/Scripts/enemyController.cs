﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour {

	public float distance;
	public Transform target;
	public bool idle;

	bool lookingRight;
	Animator anim;
	Rigidbody2D rb2d;

	//shoot and reload
	float reloadTime = 0;
	public float bulletSpeed;
	public Transform gunTip;
	public GameObject bullet;
	bool reloadCheck;

	bool firingRange;
	float minDistance = 6;
	float maxDistance = 10;

	//movement
	public float maxSpeed;

	//max Health
	public float maxHealth;
	float currentHealth;

	void Start()
	{
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();

		lookingRight = false;
		firingRange = false;
		reloadCheck = false;

		currentHealth = maxHealth;
	}

    void Update()
	{
		//Keep looking for player
		RangeCheck();

		//distance > maxDistance then enemy idle
		//distance < maxDistance then enemy moves
		//enemy fires once in firing range

		CheckMaxDistance ();

	}

	public void RangeCheck()
	{
		//This is the range distance
		distance = Vector3.Distance (transform.position, target.transform.position);
	}
		
	public void Attack()
	{
		//fire bullet
		if (Time.deltaTime > reloadTime) 
		{
			reloadTime = Time.deltaTime + bulletSpeed;

			if (lookingRight) 
			{
				Instantiate (bullet, gunTip.position, Quaternion.Euler (0, 0, 0));

				reloadCheck = true;
				Reloading ();

			} 
			else if (!lookingRight) 
			{
				Instantiate (bullet, gunTip.position, Quaternion.Euler (0, 0, 180f));

				reloadCheck = true;
				Reloading ();
			}
				
		}
		else
		{
			Reloading ();
		}
	}

	void Reloading()
	{
		//Can't fire
		reloadTime -= Time.deltaTime;

		if (Time.deltaTime > reloadTime) 
		{
			Attack ();
		} 
	}

	void CheckMaxDistance()
	{
		//distance value is checked
		//maxDistance has to be < distance

		if (distance > maxDistance) 
		{
			//enemy still looking for player
			RangeCheck ();

			//look at enemy
		}
		else if(distance < maxDistance)
		{
			//enemy will move towards player
			//enemy will fire

			if (target.transform.position.x > transform.position.x) 
			{
				lookingRight = true;
				transform.LookAt (new Vector3(target.transform.position.x, transform.position.y, transform.position.z));
				transform.rotation = new Quaternion (0, 180, 0, 0);

				//check firing range
				if (firingRange && Vector3.Distance(transform.position, target.position) <= minDistance)
				{
					firingRange = true;
					if (reloadCheck) 
					{
						Attack ();
					}
					else 
					{
						Reloading ();
					}

				}
				else if(!firingRange && Vector3.Distance(transform.position, target.position) >= minDistance)
				{
					transform.Translate (new Vector3(maxSpeed * Time.deltaTime * -1, 0 , 0));
				}

			}
			else if (target.transform.position.x < transform.position.x) 
			{
				lookingRight = false;
				transform.LookAt (new Vector3(target.transform.position.x, transform.position.y, transform.position.z));
				transform.rotation = new Quaternion (0, 0, 0, 0);

				//check firing range
				if (firingRange && Vector3.Distance(transform.position, target.position) <= minDistance) 
				{
					firingRange = true;
					if (reloadCheck) 
					{
						Attack ();
					}
					else
					{
						Reloading ();
					}
				} 
				else if(!firingRange && Vector3.Distance(transform.position, target.position) >= minDistance)
				{
					transform.Translate (new Vector3(maxSpeed * Time.deltaTime * -1, 0 , 0));
				}
			}
		}
	}

	public void TakeDamage(float damage)
	{
		currentHealth -= damage;
	}

	public float GetHealth()
	{
		return currentHealth;
	}
}
