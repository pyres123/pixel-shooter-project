﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	public float maxHealth;
	float currentHealth;
	public float maxSpeed;
	public float jumpPower;

	Rigidbody2D rb2d;
	Animator anim;
	bool faceDir;

	bool playerShoots;
	public bool isGrounded;

	//for shooting
	public Transform gunTip;
	public GameObject bullet;
	public float fireRate;
	float nextFire = 0f;

	//looking up
	 public bool lookUp;

	// Use this for initialization
	void Start ()
	{
		//initialize here
		rb2d = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator> ();

		faceDir = true;

		currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//TODO remove double jump
		anim.SetBool ("isGrounded", isGrounded);

		if (Input.GetButtonDown ("Jump") && isGrounded) 
		{
			rb2d.AddForce (Vector2.up * jumpPower); 
		}

		//player shooting
		if(Input.GetKeyDown(KeyCode.X))
		{
			anim.SetBool ("playerShoots", playerShoots);
			FireBullet ();
		}
	}

	void FixedUpdate()
	{
		//when facing up bullet should be shot upwards

		//calls the player input
		float move = Input.GetAxis ("Horizontal");

		playerMovement (move);

		//check player direction and flip
		if (move > 0 && !faceDir) 
		{
			//Flip character
			flip();
		} 
		else if(move < 0 && faceDir)
		{
			//Flip
			flip();
		}
	}

	void playerMovement(float move)
	{
		//Control player moving in this method
		rb2d.velocity = new Vector2(move * maxSpeed, rb2d.velocity.y);
		anim.SetFloat ("speed", Mathf.Abs (move));
	}

	//change player direction
	void flip()
	{
		//we flip the character here
		//using x value to flip by multiply -1
		faceDir = !faceDir;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void FireBullet()
	{
		//Check fire time elapsed
		if(Time.time > nextFire)
		{
			playerShoots = true;
			//Fire!
			nextFire = Time.time + fireRate;
			if (faceDir)
			{
				//The y cordinate will make the bullet to rotate along it's axis
				Instantiate (bullet, gunTip.position, Quaternion.Euler(new Vector3(0,0,0)));
			} 
			else if (!faceDir) 
			{
				Instantiate (bullet, gunTip.position, Quaternion.Euler(new Vector3(0,0,180f)));
			}
		}
	}

	public void TakeDamage(float bulletDamage)
	{
		//attack damage from bullet
		currentHealth -= bulletDamage;
	}

	public float GetHealth()
	{
		return currentHealth;
	}
}
