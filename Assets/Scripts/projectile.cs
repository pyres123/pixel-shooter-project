﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour {

	public float projectileSpeed;

	Rigidbody2D myRb;

	// Use this for initialization
	void Awake () 
	{
		myRb = GetComponent<Rigidbody2D> ();

		if (transform.localRotation.z > 0) {
			//straight line in x direction
			//controls the force direction of the bullet
			myRb.AddForce (new Vector2 (-1, 0) * projectileSpeed, ForceMode2D.Impulse);
		} 
		else 
		{
			myRb.AddForce (new Vector2 (1, 0) * projectileSpeed, ForceMode2D.Impulse);
		}
			
		//I want the bullet to be shot upwards but only for player
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Player") 
		{
			playerController player = coll.gameObject.GetComponent<playerController> ();
			player.TakeDamage (10f);
			Destroy (gameObject);

			if (player.GetHealth () <= 0f) {
				Destroy (coll.gameObject);
			}
		} 
		else if (coll.gameObject.tag == "Enemy") 
		{
			enemyController enemy = coll.gameObject.GetComponent<enemyController> ();
			enemy.TakeDamage (10f);
			Destroy (gameObject);

			if (enemy.GetHealth () <= 0f) {
				Destroy (coll.gameObject);
			}
		} 
		else if (coll.gameObject.tag == "Wall") 
		{
			Destroy (gameObject);
		}
	}
}
