﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyProjectile : MonoBehaviour {

	public float lifeTime;

	// Use this for initialization
	void Awake () 
	{
		Destroy (gameObject, lifeTime);
	}
}
