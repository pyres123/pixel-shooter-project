﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyFireRange : MonoBehaviour {


	enemyController enemy;
	bool isLeft = false;

	void Awake()
	{
		enemy = gameObject.GetComponentInParent<enemyController> ();
	}
		
	void OnTriggerStay2D(Collider2D coll)
	{
		if (coll.CompareTag ("Player")) 
		{
			if (isLeft) {
				enemy.Attack ();
			} 
			else
			{
				enemy.Attack ();
			}
		}
	}
}
