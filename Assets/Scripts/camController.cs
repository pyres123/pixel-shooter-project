﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camController : MonoBehaviour {

	//what the camera is following
	public Transform target;
	public float smoothing;

	Vector3 offset;

	//lowest point on the y axis
	float lowY;

	// Use this for initialization
	void Start () 
	{
		offset = transform.position - target.position;

		lowY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void FixedUpdate()
	{
		//adding the difference for point of translation
		Vector3 targetCamPos = target.position + offset;

		//Lerp allows smooth translation in built call
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);

		if (transform.position.y < lowY) 
		{
			transform.position = new Vector3 (transform.position.x, lowY, transform.position.z);
		}
	}
}
